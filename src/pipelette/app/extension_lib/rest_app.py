from __future__ import annotations

import logging
import asyncio

from fastapi import FastAPI, APIRouter
import uvicorn
import aiohttp

from ..extensions.extension import app_extension, Extension

from ..settings.models import BaseSettingsModel

logger = logging.getLogger(__name__)


class RESTAppSettings(BaseSettingsModel):
    port: int = 9099
    host: str = "localhost"
    log_level: int = logging.DEBUG


app_router = APIRouter()


@app_router.get("/close", tags=["App Control"])
async def app_close():
    RESTApp.get().close_app()


@app_router.get("/show_docs", tags=["Help"])
async def app_show_docs():
    RESTApp.get().open_swagger_in_browser()


@app_extension
def add_extension(app):
    return RESTApp


class AppClient(object):
    def __init__(self, rest_app, timeout=1.0):
        super(AppClient, self).__init__()
        self._rest_app = rest_app

        self.session = aiohttp.ClientSession(
            loop=asyncio.get_event_loop(),
            timeout=aiohttp.ClientTimeout(total=timeout),
        )

    def _make_url(self, route):
        host = self._rest_app.host
        port = self._rest_app.port
        return f"http://{host}:{port}{route}"

    async def get(self, route):
        url = self._make_url(route)
        # r = await self.session.get(url)
        # return await r.json()
        async with self.session.get(url) as r:
            data = await r.json()
        return data


class RESTApp(Extension):
    """
    This extension let other exension extend the app's REST api.
    """

    @classmethod
    def extension_package_name(cls):
        return "pipelette"

    @classmethod
    def settings_model(cls):
        return RESTAppSettings

    def init(self):
        self._fastapp = FastAPI(title="Pipelette App API")
        self.host = None
        self.port = None

        # We can't create the client until the final event loop is built, so:
        self._app_client = None

    def install(self):
        pass

    def uninstall(self):
        pass

    def _on_app_ready(self):
        self.include_router(app_router, "/app")
        self._app.get_extension("AsyncLoop").add_async_task(self._start_app)

    def _on_app_run(self):
        pass

    def include_router(self, router, prefix):
        if prefix and not prefix.startswith("/"):
            prefix = f"/{prefix}"
        self._fastapp.include_router(router, prefix=prefix)

    def remove_router(self, router):
        del self._fastapp.router[router]

    async def _start_app(self):
        # bake used settings, changing them won't change us:
        self.host = self._settings.host
        self.port = self._settings.port

        # This is derived from uvicorn.run()
        config = uvicorn.Config(
            self._fastapp,
            host=self.host,
            port=self.port,
            log_level=self._settings.log_level,
            # access_log=False,
            use_colors=False,  # :/ not on my git bash anyway...
            # Those 2 are important inside a pipelette extension:
            workers=1,
            reload=False,
        )
        server = uvicorn.Server(config=config)
        await server.serve()

    def close_app(self):
        logger.info("RESTApp.close_app() requested!")
        self._app.close()

    def open_swagger_in_browser(self):
        import webbrowser

        webbrowser.open(f"http://{self._settings.host}:{self._settings.port}/docs")

    def get_app_client(self):
        if self._app_client is None:
            self._app_client = AppClient(self)

        return self._app_client
