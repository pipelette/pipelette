from __future__ import annotations

from typing import Set
import logging

from fastapi import APIRouter

from ..extensions.extension import app_extension, Extension

from ..settings.models import BaseSettingsModel


logger = logging.getLogger(__name__)


class ExtensionManagerSettings(BaseSettingsModel):
    extension_indexes: Set[str] = set()


extension_manager_rooter = APIRouter(tags=["Extensions"])


@extension_manager_rooter.get("/installed")
async def get_installed_extensions_info():
    return await ExtensionManager.get().get_installed_extensions_info()


@app_extension
def add_extension(app):
    return ExtensionManager


class ExtensionManager(Extension):
    @classmethod
    def extension_package_name(cls):
        return "pipelette"

    @classmethod
    def settings_model(cls):
        return ExtensionManagerSettings

    def init(self):
        pass

    def install(self):
        pass

    def _on_app_ready(self):
        self._app.get_extension("RESTApp").include_router(
            extension_manager_rooter, "/extensions"
        )

    def uninstall(self):
        pass

    async def get_extension_info(self, extension_name):
        extension: Extension = self._app.get_extension(extension_name)
        return dict(
            name=extension_name,
            package=extension.extension_package_name(),
            version=extension.extension_package_version(),
            module=extension.extension_module_name(),
        )

    async def get_installed_extensions_info(self):
        info = []
        extension_names = self._app.extension_names()
        for name in extension_names:
            info.append(await self.get_extension_info(name))
        return info
