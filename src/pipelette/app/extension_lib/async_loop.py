import asyncio
import logging

from ..extensions.extension import app_extension, Extension

logger = logging.getLogger(__name__)


@app_extension
def add_extension(app):
    return AsyncLoop


class AsyncLoop(Extension):
    """
    This extension enables other extensions to execute async tasks.
    """

    @classmethod
    def extension_package_name(cls):
        return "pipelette"

    def init(self):
        self._event_loop = asyncio.get_event_loop()
        self._async_tasks = []
        self._close_requested: asyncio.Future = None

    def install(self):
        pass

    def uninstall(self):
        pass

    def get_event_loop(self):
        return self._event_loop

    def set_event_loop(self, event_loop):
        self._event_loop = event_loop

    def add_async_task(self, task):
        self._async_tasks.append(task)

    def _on_app_ready(self):
        pass

    def _on_app_run(self):
        self._start_loop()

    def _on_app_closed(self):
        self._stop()

    def _start_loop(self):
        logger.info("AsyncLoo._on_run()")
        loop = self._event_loop
        for task in self._async_tasks:
            logger.info("  creating async task: %s", task)
            loop.create_task(task())
        self._close_requested = asyncio.Future()
        self._event_loop.run_until_complete(self._close_requested)
        logger.info("async run finished.")

    def _stop(self):
        """
        Stops the event loop, which would most probably stop the whole app.
        """
        if self._close_requested is None or self._close_requested.done():
            logger.info("Cannot request stop, not alive !")
            return
        self._close_requested.set_result(True)
