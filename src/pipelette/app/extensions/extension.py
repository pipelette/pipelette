from __future__ import annotations
import logging
import abc
import pkg_resources

import pluggy

from ..settings.models import BaseSettingsModel

logger = logging.getLogger(__name__)

app_extension = pluggy.HookimplMarker("pipelette.app.extension")
"""Marker to declare an app extension."""


class Extension(abc.ABC):
    _INSTANCE = None

    @classmethod
    def get(cls) -> "Extension":
        """
        Returns the last instance created.
        """
        return cls._INSTANCE

    @classmethod
    def extension_name(cls):
        return cls.__name__

    @classmethod
    @abc.abstractmethod
    def extension_package_name(cls):
        """
        Returns the name of the package to install in order
        to make this extension available.

        This cannot be guessed 100% accurately in case of
        package namespaces so each extension must implement
        this.
        """

    @classmethod
    def extension_package_version(cls):
        """
        Returns the version of the package which made this extension
        available.
        """
        return pkg_resources.get_distribution(cls.extension_package_name()).version

    @classmethod
    def extension_module_name(cls):
        """
        Returns the module declaring this extension.

        Default is to return the module containing the implementation
        of the Extension, but if your `@app_extension` decorator is
        used in another module you must provide this module name instead.
        """
        return cls.__module__

    @classmethod
    def wants_to_override(cls, other_extension_types):
        """
        Returns True if this extension wants to override all
        other ones in `other_extension_types`.

        The extension manager will check this only if both
        `extension_name()` return the same value.

        Default: True
        """
        return True

    @classmethod
    def allows_override(cls, other_extension_cls):
        """
        Returns True if this extension allow this other one
        to override it.

        Default: False
        """
        return False

    @classmethod
    def settings_model(cls):
        """
        Must return None or a subclass of `pipelette.settings.models.BaseSettingsModel`.

        Defaults to return None.
        """
        return None

    def __init__(self, app, settings: BaseSettingsModel):
        super(Extension, self).__init__()
        self.__class__._INSTANCE = self
        self._app = app
        self._settings = settings

        self.init()

    @abc.abstractmethod
    def init(self):
        """
        This will be called on extension instanciation. Override it
        intead of overriding __init__().
        """

    @abc.abstractmethod
    def install(self):
        print("Installing extension", self.extension_name())

    @abc.abstractmethod
    def uninstall(self):
        print("Installing extension", self.extension_name())

    def _on_app_ready(self):
        """
        Called when all extensions have been installed.
        """

    def _on_app_run(self):
        """
        Called after all extensions are ready.

        A good place to start a blocking loop, but only one
        extension should do it...
        """

    def _on_app_closing(self):
        """
        Called when application request closing.
        If no extension prevents it, a call to _on_app_closed() will follow.

        Subclasses must return True to prevent application close.
        """
        return False

    def _on_app_closed(self):
        """
        Called when application requested closing and no
        extension prevented it.
        """
