from typing import List, Type
import logging
import pluggy
from collections import defaultdict

from ..settings.registry import SettingsRegistry

from . import hookspecs
from .extension import Extension


logger = logging.getLogger(__name__)

ENTRY_POINT_NAME = "pipelette.app.extension"


class ExtensionRegistry(object):
    def __init__(self, app, settings_manager):
        super(ExtensionRegistry, self).__init__()
        self._app = app
        self._settings_manager: SettingsRegistry = settings_manager
        self._mgr = pluggy.PluginManager(ENTRY_POINT_NAME)
        self._mgr.add_hookspecs(hookspecs)

        self._extension_types: List[Type[Extension]] = []
        self._extensions: List[Extension] = []

    def register_plugins(self, plugins_namespace):
        logger.info(f"Registering plugins from {plugins_namespace}")
        self._mgr.register(plugins_namespace)

    def discover_plugins(self):
        logger.info(f"Discovering plugins from entry point {ENTRY_POINT_NAME!r}")
        self._mgr.load_setuptools_entrypoints(ENTRY_POINT_NAME)

    def collect_extension_types(self):
        logger.info("Collecting Extensions from `add_extension()` plugins hooks.")
        extension_classes = self._mgr.hook.add_extension(app=self._app)
        self._extension_types = extension_classes
        logger.info("Collected Extensions:")
        for extension_class in self._extension_types:
            logger.info(f"  {extension_class.extension_name()}: {extension_class}")

        self._extension_types = self.dedoubled_extension_types()

        logger.info("Final Extensions list:")
        for extension_class in self._extension_types:
            logger.info(f"  {extension_class.extension_name()}: {extension_class}")

    def dedoubled_extension_types(self) -> List[Type[Extension]]:
        """
        Returns a list of filter out extension types so that only one extension remains
        for each `extension_name`.

        The filtering is based on `Extension.wants_to_override()` and `Extension.allow_override()`
        """
        logger.info("Dedoubling Extensions")
        allowed_extension_types = []
        by_name = defaultdict(list)
        for extension_type in self._extension_types:
            allow = True
            overriddens: List[Type[Extension]] = by_name[
                extension_type.extension_name()
            ]
            if overriddens:
                logger.info(f"  Extension {extension_type} overrides {overriddens}")
                if extension_type.wants_to_override(overriddens):
                    logger.info(f"     Override wanted by {extension_type}.")
                    for overriden in overriddens:
                        if not overriden.allows_override(extension_type):
                            logger.info(f"     override not allowed by {overriden}")
                            allow = False
                            break
                    if allow:
                        logger.info(f"    Override allowed, using {extension_type}")
                else:
                    logger.info(f"  Override not wanted by {extension_type}")
            if allow:
                allowed_extension_types.append(extension_type)
            by_name[extension_type.extension_name()].append(extension_type)

        return allowed_extension_types

    def install_all_extensions(self):
        logger.info("Installing all extensions")
        # TODO: uninstall all first ?
        for ExtensionType in self._extension_types:
            logger.info(f"  Creating {ExtensionType.extension_name()}")
            settings = self._settings_manager.register_settings(
                ExtensionType.extension_name(), ExtensionType.settings_model()
            )
            extension = ExtensionType(self._app, settings)
            self._extensions.append(extension)
            extension.install()

    def on_app_start(self):
        logger.info("Calling _on_app_ready() on all extensions...")
        for extension in self._extensions:
            extension._on_app_ready()

        logger.info("Calling _on_run() on all extensions...")
        for extension in self._extensions:
            extension._on_app_run()

    def on_app_close(self):
        logger.info("Closing all extensions")
        prevented = False
        for extension in self._extensions:
            logger.info(f"  {extension.extension_name()}")
            prevented = extension._on_app_closing()
            if prevented:
                logger.info("    prevented closing!")
                return
        if not prevented:
            for extension in self._extensions:
                extension._on_app_closed()

    def extension_names(self):
        return [extension.extension_name() for extension in self._extensions]

    def get_extension(self, extension_name):
        for extension in self._extensions:
            if extension.extension_name() == extension_name:
                return extension
