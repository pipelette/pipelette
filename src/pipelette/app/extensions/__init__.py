"""
pipelette.app.extensions - it's a little bit meta :p

We're using pluggy to discover extensions in the 'pipelette.app.extension' entry point.
But our extension are not really pluggy plugins. We're only using pluggy to collect a 
list of extension to use.

So:
- plugins are discoverable
- a plugin can contain several extension.
- plugins only declare an `add_extension` hook per extension


Some of the reason why we're not simply using pluggy:
- We need our extensions to be overridable by other extensions, 
but pluggy cannot order its plugins more than with `trylast`/`tryfirst`. 
- We need our extensions to be activable/deactivable during runtime, 
but pluggy only allow disabling at discovery time.
- We need our extensions to be "hot reloadabel" because the system must be able to update the extension versions, 
but pluggy has no such concept.


NOTE: using pluggy is an implemetation detail and does not bleed (to much) outside of `pipelette.app.extension`.
We may very well decide in the future to read entry-points ourselves instead of using pluggy.

"""
