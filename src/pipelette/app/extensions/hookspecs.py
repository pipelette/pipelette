from typing import Type

import pluggy

from .extension import Extension


app_extension_spec = pluggy.HookspecMarker("pipelette.app.extension")


@app_extension_spec
def add_extension(app) -> Type[Extension]:
    """Return an extension to register to the app.

    :param app: the app looking for extensions. You can inspect it to
    decide which extensions you want to register.

    :return: a subclasses of `pipelette.app.extensions.extension.Extension`.
    """
