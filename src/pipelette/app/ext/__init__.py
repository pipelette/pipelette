"""
This package is a namespace for official `pipelette.app` extension plugins.
"""

__path__ = __import__("pkgutil").extend_path(__path__, __name__)
