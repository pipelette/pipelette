from typing import Type, Dict

from .models import BaseSettingsModel


class SettingsRegistry(object):
    def __init__(self, app):
        super(SettingsRegistry, self).__init__()
        self._app = app

        self._settings: Dict[str, BaseSettingsModel] = {}

    def get_env_file(self):
        return "./settings.env"

    def register_settings(self, name: str, model: BaseSettingsModel | None):
        """
        Initialize the settings model, store it under `name` and return it.

        The value in the returned settings will be derived from the default
        value, the environement variables, and the configured dot-env file.
        """
        model = model or BaseSettingsModel
        settings = model(_env_file=self.get_env_file())
        self._settings[name] = settings
        return settings

    def get(self, name):
        return self._settings[name]
