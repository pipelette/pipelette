import pydantic

from .utils import json_dumps, json_loads


class BaseSettingsModel(pydantic.BaseSettings):
    class Config:
        case_sentive = False
        env_prefix = "PIPELETTE_APP_"
        env_file_encoding = "utf-8"
        # FIXME: decide if we need this:
        # json_loads = json_loads
        # json_dumps = json_dumps
