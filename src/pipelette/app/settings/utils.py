from typing import Any, Callable
import datetime
from pydantic import BaseModel

import json  # TODO: use orjson ?


def json_default_handler(value: Any) -> Any:
    if isinstance(value, BaseModel):
        return json.dumps(value.dict())

    if isinstance(value, datetime.datetime):
        return value.isoformat()

    raise TypeError(f"Type {type(value)} is not JSON serializable")


def json_dumps(data: Any, *, default: Callable[[Any], Any] | None = None) -> str:
    return json.dumps(data, default=json_default_handler).decode()


def json_loads(data: str) -> Any:
    return json.loads(data)
