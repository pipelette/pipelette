"""

The `pipelette.app` package let you build desktop app by providing a list of `pipelette.app.plugin`.

"""
from .extensions.extension import Extension
from .extensions.registry import ExtensionRegistry
from .settings.models import BaseSettingsModel
from .settings.registry import SettingsRegistry

from .extension_lib import rest_app, async_loop, extension_manager


class PipeletteApp(object):
    def __init__(self, args):
        super(PipeletteApp, self).__init__()
        self._settings = SettingsRegistry(self)
        self._extensions = ExtensionRegistry(self, self._settings)
        self._setup_extensions()

    def _setup_extensions(self):
        self._extensions.register_plugins(rest_app)
        self._extensions.register_plugins(async_loop)
        self._extensions.register_plugins(extension_manager)

        self._extensions.discover_plugins()
        self._extensions.collect_extension_types()
        self._extensions.install_all_extensions()

    def extension_names(self):
        return self._extensions.extension_names()

    def get_extension(self, extension_name) -> Extension:
        return self._extensions.get_extension(extension_name)

    def get_settings(self, extension_name) -> BaseSettingsModel:
        return self._settings.get(extension_name)

    def set_notifier(self, notifier):
        """
        Sets the callback used by `notify()`.

        The notifier must be a callable with the same
        signature as `notify()`.
        """
        self._notifier = notifier

    def notify(self, title, message):
        self._notifier(title, message)

    def start(self):
        self._extensions.on_app_start()

    def close(self):
        self._extensions.on_app_close()


def main():
    """
    This is a temp thingy to test stuff...
    """
    import sys
    import logging

    logging.basicConfig(level=logging.DEBUG)

    app = PipeletteApp(sys.argv)

    app.start()
