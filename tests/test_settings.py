import os

import pytest
import pathlib

from pydantic import Field

from pipelette.app.settings.models import BaseSettingsModel


@pytest.fixture
def test_model():
    class MySettings(BaseSettingsModel):
        bob: int = 2

    return MySettings


def test_env_override(test_model: BaseSettingsModel):
    os.environ["PIPELETTE_APP_BOB"] = "5"

    settings = test_model()
    assert settings.bob == 5


def test_dotenv_file_override(test_model: BaseSettingsModel, tmp_path: pathlib.Path):
    content = """
# Test dotenv file
PIPELETTE_APP_BOB=10
"""
    dotenv_filename = tmp_path / "test.env"
    dotenv_filename.write_text(content)

    # Ensure the env var don't shadow ou dotenv file:
    del os.environ["PIPELETTE_APP_BOB"]

    settings = test_model(_env_file=dotenv_filename)
    assert settings.bob == 10
